require "./2.rb"

describe Debug1202ProgramAlarm do
  let(:input) { [1,0,0,3,1,1,2,3,1,3,4,3,1,5,0,3,2,1,9,19,1,5,19,23,1,6,23,27,1,27,10,31,1,31,5,35,2,10,35,39,1,9,39,43,1,43,5,47,1,47,6,51,2,51,6,55,1,13,55,59,2,6,59,63,1,63,5,67,2,10,67,71,1,9,71,75,1,75,13,79,1,10,79,83,2,83,13,87,1,87,6,91,1,5,91,95,2,95,9,99,1,5,99,103,1,103,6,107,2,107,13,111,1,111,10,115,2,10,115,119,1,9,119,123,1,123,9,127,1,13,127,131,2,10,131,135,1,135,5,139,1,2,139,143,1,143,5,0,99,2,0,14,0] }

  it 'returns right answer on part one' do
    # Before running the program, replace position 1 with the
    # value 12 and replace position 2 with the value 2.
    cpu = Debug1202ProgramAlarm.run(input, 12, 2)
    # What value is left at position 0 after the program halts?
    expect(cpu.state[0]).to eq(3562672)
  end

  it 'returns right answer on part two' do
    # Find the input noun and verb that cause the program to produce the output 19690720. What is 100 * noun + verb?
    result = Debug1202ProgramAlarm.bruteforce(input, 19690720)
    expect(result).to eq(8250)
  end
end
