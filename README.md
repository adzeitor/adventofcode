Run all tests:

```bash
$ rspec lib/*.rb *.rb
```

Run tests for specifi puzzle

```bash
$ rspec 2_spec.rb
```
