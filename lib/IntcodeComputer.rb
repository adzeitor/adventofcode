class IntcodeComputer
  attr_accessor :state

  def initialize(prog)
    @pos = 0
    @state = prog.dup
    @halted = false
  end

  def step
    if opcode == 1
      set_result(operand1 + operand2)
      forward
    elsif opcode == 2
      set_result(operand1 * operand2)
      forward
    else
      @halted = true
    end
    self
  end

  def run
    until halted? do
      step
    end
    self
  end

  def halted?
    @halted
  end
  
  private
  def opcode
    @state[@pos]
  end
  
  def operand1
    @state[@state[@pos+1]]
  end
  
  def operand2
    @state[@state[@pos+2]]
  end
  
  def set_result(value)
    @state[@state[@pos+3]] = value
  end

  def forward(n=4)
    @pos += n
  end
end
