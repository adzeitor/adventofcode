require "IntcodeComputer"

describe IntcodeComputer do
  it 'steps on opcode 1 (adding)' do
    machine = IntcodeComputer.new([1, 0, 0, 0, 99]).step
    expect(machine.state).to eq([2, 0, 0, 0, 99])
  end

  it 'steps on opcode 2 (multiplying)' do
    machine = IntcodeComputer.new([2,3,0,3,99]).step
    expect(machine.state).to eq([2,3,0,6,99])
  end

  it 'halted on opcode 99 (halt)' do
    machine = IntcodeComputer.new([99]).run
    expect(machine).to be_halted
  end

  it 'runs example' do
    machine = IntcodeComputer.new(
      [1,9,10,3,
       2,3,11,0,
       99,
       30,40,50,
      ]).run
    expect(machine.state).to eq([3500, 9, 10, 70,
                               2, 3, 11, 0,
                               99,
                               30, 40, 50])
  end

  it 'runs another example' do
    machine = IntcodeComputer.new([1,1,1,4,99,5,6,0,99]).run
    expect(machine.state).to eq([30,1,1,4,2,5,6,0,99])
  end

  it 'needs not modify prog array' do
    prog = [1,1,1,4,99,5,6,0,99]
    machine = IntcodeComputer.new(prog).run
    expect(prog).to eq([1,1,1,4,99,5,6,0,99])
  end
end
