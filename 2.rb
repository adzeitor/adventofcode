require "./lib/IntcodeComputer.rb"

class Debug1202ProgramAlarm
  def self.run(prog, noun, verb)
    prog[1] = noun
    prog[2] = verb
    return IntcodeComputer.new(prog).run()
  end

  def self.bruteforce(prog, output)
    # Each of the two input values will be between 0 and 99, inclusive.
    (0..99).each do |noun|
      (0..99).each do |verb|
        if self.run(prog, noun, verb).state[0] == output
          return 100 * noun + verb
        end
      end
    end
  end
end

